package com.example.epicture;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public final class MyRequest {

    public final static void getGallery(Context context, final VolleyCallback volleyCallback) {
        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.GET, Url.GalleryUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        volleyCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error", "error");
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Client-ID 3b0d77918c4c21e");
                return headers;
            }
        };
        Singleton.getInstance(context).addToRequestQueue(objectRequest);
    }

    public final static void getFavories(Context context, final Map<String, String> headers, final VolleyCallback volleyCallback, String username) {
        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.GET, Url.FavoriesUrl + username + "/favorites", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        volleyCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Favoris_error", "Error");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                return headers;
            }
        };
        Singleton.getInstance(context).addToRequestQueue(objectRequest);
    }

    public final static void getImageInfo(Context context, final VolleyCallback volleyCallback, String imageHash) {
        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.GET, Url.ImageUrl + imageHash, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        volleyCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Favoris_error", "Error");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Client-ID 3b0d77918c4c21e");
                return headers;
            }
        };
        Singleton.getInstance(context).addToRequestQueue(objectRequest);
    }

    public final static void uploadImage(Context context, final VolleyCallback volleyCallback, final String url, final String access_token) {
        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST, Url.UploadUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        volleyCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Upload_error", "Error");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
            @Override
            public byte[] getBody() {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                DataOutputStream conv = new DataOutputStream(baos);
                try {
                    conv.write(url.getBytes());
                    baos.flush();
                    baos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return baos.toByteArray();
            }
        };
        Singleton.getInstance(context).addToRequestQueue(objectRequest);
    }

    public final static void updateImageUpload(Context context, final VolleyCallback volleyCallback, final ImageInfo imageInfo, final String Access_token) {
        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST, Url.ImageUrl + imageInfo.getId(), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        volleyCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Upload_error", "Error");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + Access_token);
                return headers;
            }
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("title", imageInfo.getTitle());
                params.put("description", imageInfo.getDescription());
                return params;
            }
        };
        Singleton.getInstance(context).addToRequestQueue(objectRequest);
    }

    public final static void setImageFavories(Context context, final VolleyCallback volleyCallback, ImageInfo info, final String access_token) {
        if (info.getAlbumd() == null)
            return;
        String urlType = (info.getAlbumd().contains("true")) ? Url.AlbumUrl : Url.ImageUrl;
        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST, Url.AlbumUrl + info.getId() + "/favorite", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        volleyCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Favories_error", "Error");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };
        Singleton.getInstance(context).addToRequestQueue(objectRequest);
    }

    public final static void getImageAccount(Context context, final VolleyCallback volleyCallback, final String access_token) {
        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.GET, Url.myAccount, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        volleyCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Account_error", "Error");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };
        Singleton.getInstance(context).addToRequestQueue(objectRequest);
    }
}
