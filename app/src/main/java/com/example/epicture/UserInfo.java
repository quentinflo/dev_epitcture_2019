package com.example.epicture;

import java.io.Serializable;

public class UserInfo implements Serializable {
    private String access_token;
    private String refresh_token;
    private String username;
    private String id;

    public void setAccessToken(String access_token) {
        this.access_token = access_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccess_token() {
        return this.access_token;
    }

    public String getRefresh_token() {
        return this.refresh_token;
    }

    public String getUsername() {
        return this.username;
    }

    public String getId() {
        return this.id;
    }
}
