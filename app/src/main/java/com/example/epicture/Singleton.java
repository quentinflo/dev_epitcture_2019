package com.example.epicture;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class Singleton {
    private static Singleton instance;
    private static Context _context;
    private RequestQueue queue;

    private Singleton(Context context) {
        _context = context;
        queue = getRequestQueue();
    }

    public RequestQueue getRequestQueue() {
        if (queue == null)
            queue = Volley.newRequestQueue(_context.getApplicationContext());
        return queue;
    }

    public static synchronized Singleton getInstance(Context context) {
        if (instance == null)
            instance = new Singleton(context);
        return instance;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}
