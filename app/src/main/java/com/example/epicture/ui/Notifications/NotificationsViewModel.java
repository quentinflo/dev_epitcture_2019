package com.example.epicture.ui.Notifications;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import androidx.lifecycle.ViewModel;

import com.example.epicture.ImageInfo;
import com.example.epicture.NavigationActivity;
import com.example.epicture.R;
import com.example.epicture.MyRequest;
import com.example.epicture.VolleyCallback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NotificationsViewModel extends ViewModel {

    private ImageInfo imageInfo;
    private List<ImageInfo> imageInfoList;

    public NotificationsViewModel() {
    }

    public void uploadImage(final Context context, String url, final View view) {
        imageInfoList = new ArrayList<>();
        MyRequest.uploadImage(context, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                try {
                    imageInfo = new ImageInfo();
                    JSONObject obj = result.getJSONObject("data");
                    imageInfo.setId(obj.getString("id"));
                    imageInfo.setTitle(obj.getString("title"));
                    imageInfo.setDescription(obj.getString("description"));
                    imageInfo.setType(obj.getString("type").substring(obj.getString("type").indexOf("/"), obj.getString("type").length()));
                    imageInfo.setLink(obj.getString("link"));
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
                uploadOnImgur();
            }

            private void uploadOnImgur() {
                ImageView imageView = view.findViewById(R.id.uploadId);
                Picasso.get().load(imageInfo.getLink()).into(imageView);
            }
        }, url, NavigationActivity.getClientSettings().getAccess_token());
    }
}