package com.example.epicture.ui.profile;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.epicture.ViewAdapter;
import com.example.epicture.ImageInfo;
import com.example.epicture.NavigationActivity;
import com.example.epicture.R;
import com.example.epicture.MyRequest;
import com.example.epicture.VolleyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProfileViewModel extends ViewModel {

    private List<ImageInfo> imageInfoList;

    public ProfileViewModel() {
    }

    public void displayProfile(final Context context, final View view, final Fragment fragment) {
        imageInfoList = new ArrayList<>();
        MyRequest.getImageAccount(context, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                try {
                    Log.d("Account_result", result.toString());
                    JSONArray array = result.getJSONArray("data");
                    for (int i = 0; i < array.length(); i++) {
                        ImageInfo imageInfo;
                        imageInfo = new ImageInfo();
                        if (array.getJSONObject(i).getString("link").contains("i.imgur")) {
                            imageInfo.setLink(array.getJSONObject(i).getString("link"));
                        }
                        else {
                            imageInfo.setLink(array.getJSONObject(i).getString("id") + array.getJSONObject(i).getString("type").substring(array.getJSONObject(i).getString("type").indexOf("/"), array.getJSONObject(i).getString("type").length()));
                        }
                        imageInfo.setId(array.getJSONObject(i).getString("id"));
                        imageInfo.setTitle(array.getJSONObject(i).getString("title"));
                        imageInfo.setDescription(array.getJSONObject(i).getString("description"));
                        imageInfoList.add(imageInfo);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                getAccountImage(context, view);
            }

            private void getAccountImage(Context context, final View view) {
                RecyclerView recyclerView = view.findViewById(R.id.ProfileRecycler);
                ViewAdapter adapter = new ViewAdapter(context, imageInfoList, fragment);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            }
        }, NavigationActivity.getClientSettings().getAccess_token());
    }
}