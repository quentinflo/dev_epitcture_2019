package com.example.epicture.ui.home;

import android.content.Context;
import android.media.Image;
import android.util.Log;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.epicture.ViewAdapter;
import com.example.epicture.ImageInfo;
import com.example.epicture.R;
import com.example.epicture.MyRequest;
import com.example.epicture.VolleyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HomeViewModel extends ViewModel {

    private List<ImageInfo> imageList;

    public HomeViewModel() {
    }

    public void drawVirals(final Context context, final View view, final Fragment fragment) {
        imageList = new ArrayList<>();
        MyRequest.getGallery(context, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                try {
                    Log.d("Json_result", result.toString());
                    JSONArray array = result.getJSONArray("data");
                    for (int i = 0; i < array.length(); i++) {
                        ImageInfo imageInfo = new ImageInfo();
                        imageInfo.setType("jpg");
                        String is_album = (array.getJSONObject(i).getString("is_album").contains("true")) ? "true" : "false";
                        imageInfo.setAlbumd(is_album);
                        imageInfo.setId(array.getJSONObject(i).getString("id"));
                        if (array.getJSONObject(i).getString("link").indexOf("i.imgur") != -1) {
                            imageInfo.setLink(array.getJSONObject(i).getString("link"));
                        }
                        else {
                            imageInfo.setLink("https://i.imgur.com/" + array.getJSONObject(i).getString("cover") + "." + imageInfo.getType());
                        }
                        imageInfo.setTitle(array.getJSONObject(i).getString("title"));
                        imageInfo.setDescription(array.getJSONObject(i).getString("description"));
                        imageList.add(imageInfo);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                drawOnVirals(context, view);
            }

            private void drawOnVirals(final Context context, final View view) {
                RecyclerView recyclerView = view.findViewById(R.id.HomeRecycler);
                ViewAdapter adapter = new ViewAdapter(context, imageList, fragment);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            }
        });
    }
}