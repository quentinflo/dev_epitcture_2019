package com.example.epicture;

public class Url {

    public final static String GalleryUrl = "https://api.imgur.com/3/gallery/hot/viral";

    public final static String FavoriesUrl = "https://api.imgur.com/3/account/";

    public final static String ImageUrl = "https://api.imgur.com/3/image/";

    public final static String UploadUrl = "https://api.imgur.com/3/upload";

    public final static String AlbumUrl = "https://api.imgur.com/3/album/";

    public final static String myAccount = "https://api.imgur.com/3/account/me/images";

}
