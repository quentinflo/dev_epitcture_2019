package com.example.epicture;

import org.json.JSONObject;

public interface VolleyCallback {
    void onSuccess(JSONObject result);
}
