package com.example.epicture;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

public class ConnexionActivity extends AppCompatActivity {

    private WebView imgurView;
    private String url = "https://api.imgur.com/oauth2/authorize?client_id=22182e5411cfd75&response_type=token";
    private String redirect_url = "https://www.getpostman.com/oauth2/callback";
    private UserInfo client_settings;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppDialogueTheme);
        setContentView(R.layout.connect_layout);

        imgurView = findViewById(R.id.connect_id);
        imgurView.loadUrl(url);

        imgurView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains(redirect_url)) {
                    client_settings = getClientSettings(url, view);
                    Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                    intent.putExtra("client_settings", client_settings);
                    startActivity(intent);
                }
                else
                    view.loadUrl(url);
                return true;
            }
        });
    }

    private UserInfo getClientSettings(String url, WebView view) {
        UserInfo clientSettings = new UserInfo();

        int access_index = url.indexOf("access_token=");
        int refresh_index = url.indexOf("refresh_token=");
        int username_index = url.indexOf("account_username=");
        int id_index = url.indexOf("account_id=");
        String access_token = url.substring(access_index + "access_token=".length(), url.indexOf("&expires_in"));
        String refresh_token = url.substring(refresh_index + "refresh_token=".length(), url.indexOf("&account"));
        String username = url.substring(username_index + "account_username=".length(), url.indexOf("&account_id"));
        String id = url.substring(id_index + "account_id=".length());
        clientSettings.setAccessToken(access_token);
        clientSettings.setRefresh_token(refresh_token);
        clientSettings.setUsername(username);
        clientSettings.setId(id);
        return clientSettings;
    }
}
