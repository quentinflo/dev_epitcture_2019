package com.example.epicture;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.List;

public class ViewAdapter extends RecyclerView.Adapter<ViewAdapter.myViewHolder>{

    Context mContext;
    List<ImageInfo> mData;
    Fragment fragment;

    public ViewAdapter(Context mContext, List<ImageInfo> mData, Fragment fragment) {
        Log.d("InCardView", "im in");
        this.mContext = mContext;
        this.mData = mData;
        this.fragment = fragment;
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View v = inflater.inflate(R.layout.cardlist, parent, false);
        return new myViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final myViewHolder holder, final int position) {
        Picasso.get().load(mData.get(position).getLink()).resize(340, 300).into(holder.background_img, new Callback() {
            @Override
            public void onSuccess() {
                holder.background_img.setVisibility(View.VISIBLE);
                holder.progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onError(Exception e) {
                holder.background_img.setVisibility(View.INVISIBLE);
                holder.progressBar.setVisibility(View.VISIBLE);
            }
        });
        final Dialog dialog;
        dialog = new Dialog(mContext);

        holder.background_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Button addOnFav;
                ImageView selectedImage;
                dialog.setContentView(R.layout.pop_up);
                addOnFav = (Button) dialog.findViewById(R.id.add_favories);
                selectedImage = (ImageView) dialog.findViewById(R.id.image_info);
                selectedImage.setImageDrawable(holder.background_img.getDrawable());
                addOnFav.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MyRequest.setImageFavories(mContext, new VolleyCallback() {
                            @Override
                            public void onSuccess(JSONObject result) {
                                Log.d("isFavorite", result.toString());
                            }
                        }, mData.get(position), NavigationActivity.getClientSettings().getAccess_token());
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class myViewHolder extends RecyclerView.ViewHolder {

        ImageView background_img;
        ProgressBar progressBar;

        public myViewHolder(View itemView) {
            super(itemView);

            background_img = itemView.findViewById(R.id.card_background);
            progressBar = itemView.findViewById(R.id.CardProgress);
        }
    }
}
