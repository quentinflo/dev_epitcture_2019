package com.example.epicture;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class Upload {

    private String url;
    private ImageView view;

    public Upload(String url, ImageView view) {
        this.url = url;
        this.view = view;
        this.loadImage();
    }

    private void loadImage() {
        Picasso.get().load(this.url).into(this.view);
    }
}
