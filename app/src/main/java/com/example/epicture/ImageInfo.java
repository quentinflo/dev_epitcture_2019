package com.example.epicture;

public class ImageInfo {

    private String title;
    private String description;
    private String id;
    private String type;
    private String link;
    private String album;

    public void setAlbumd(String album) {
        this.album = album;
    }

    public String getAlbumd() {
        return album;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {return this.title;}

    public String getDescription() {return this.description;}

    public String getId() {return this.id;}

    public String getType() {return this.type;}

    public String getLink() {return this.link;}
}
